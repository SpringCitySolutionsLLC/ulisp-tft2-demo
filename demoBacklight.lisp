; demoBacklight.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Demonstrate backlight
; cycles thru brightness levels

(defun demoBacklight nil

  (tft2:init)

  (dotimes (brightness 10)
    (tft2:backlight (* (- 10 brightness) 10))
    (delay 300)
    )

  (dotimes (brightness 10)
    (tft2:backlight (* brightness 10))
    (delay 300)
    )

  (tft2:backlight 50)

  t
  )

; end of demoBacklight.lisp file
