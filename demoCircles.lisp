; demoCircles.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Clear the screen to black
; Then draw 15 random triangles of random colors

(defun demoCircles nil

  (tft2:init)

  (gfx:clear-screen 0 0 0)

  (dotimes (count 15)
    (gfx:circle-filled (random *display:x-max*) (random *display:y-max*) (random 50)
                         (random 255) (random 255) (random 255)  )
    )

  (eval t)

  )

; end of demoCircles.lisp file
