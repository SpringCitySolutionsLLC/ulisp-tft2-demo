; demoButtons.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Demonstrate buttons and joystick UI via polling
; Not terribly fast, takes around a second to poll eight contacts.

(defun demoButtons nil

  (tft2:init)

  (dotimes (x 30)
    (when (tft2:button-a-p) (print 'ButtonA))
    (when (tft2:button-b-p) (print 'ButtonB))
    (when (tft2:button-c-p) (print 'ButtonC))

    (when (tft2:joy-north-p) (print 'JoyNorth))
    (when (tft2:joy-east-p) (print 'JoyEast))
    (when (tft2:joy-south-p) (print 'JoySouth))
    (when (tft2:joy-west-p) (print 'JoyWest))
    (when (tft2:joy-down-p) (print 'JoyDown))

    (delay 50)
    )

  (eval t)

  )

; end of demoButtons.lisp file
