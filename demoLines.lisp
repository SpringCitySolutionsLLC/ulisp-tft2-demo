; demoLines.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Clear the screen to black
; Then draw 30 random position, length, and color lines

(defun demoLines nil

  (tft2:init)

  (gfx:clear-screen 0 0 0)

  (dotimes (count 30)
    (gfx:line (random *display:x-max*) (random *display:y-max*)
              (random *display:x-max*) (random *display:y-max*)
              (random 255) (random 255) (random 255)  )
    )

  (eval t)

  )

; end of demoLines.lisp file
