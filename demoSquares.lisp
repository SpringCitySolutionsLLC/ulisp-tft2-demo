; demoSquares.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Clear the screen to a random RGB color
; Then draw 50 tiny squares on the screen at
; random placement and random color

(defun demoSquares nil

  (tft2:init)

  (gfx:clear-screen (random 255) (random 255) (random 255))

  (dotimes (count 50)
    (let ((x 0)
          (y 0))
      (setq x (random (- *display:x-max* 10)))
      (setq y (random (- *display:y-max* 10)))
      (gfx:rectangle-filled x y
                            (+ x 10) (+ y 10)
                            (random 255) (random 255) (random 255))
      )
    )

  (eval t)

  )

; end of demoSquares.lisp file
