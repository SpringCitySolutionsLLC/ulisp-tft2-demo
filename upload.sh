#!/usr/bin/env bash
# upload.sh
# (C) 2019 Spring City Solutions LLC
# MIT License, see LICENSE file

# upload.sh generates two files, both included in .gitignore:
#
# upload.txt is the complete collection of functions ready
# to "text upload" or "send file" to the uLisp controller
# including no comments or blank lines.
#
# upload-with-comments.txt is a human readable complete
# collection of functions.

rm -f ./upload.txt ./upload-with-comments.txt

echo "; Output from upload.txt generated on `date`" > ./upload-with-comments.txt
echo ";" >> ./upload-with-comments.txt

# GFX library
cat ulisp-gfx/*.lisp >> ./upload-with-comments.txt

# Seesaw library
cat ulisp-seesaw/*.lisp >> ./upload-with-comments.txt

# ST7735R library
cat ulisp-st7735r/*.lisp >> ./upload-with-comments.txt

# TFTv2 library
cat ulisp-tft2/*.lisp >> ./upload-with-comments.txt

# The demos in this repo
cat ./*.lisp >> ./upload-with-comments.txt

# sed eats comment lines making text uploads 
# to the uLisp controller much faster.
# Also the uLisp repl reader gets highly agitated by line constructs like:
# ; Example (blah)
# and considers (blah) to not be part of a comment.

# Also get rid of whitespace only blank lines

cat upload-with-comments.txt | sed '/^;/ d' | sed '/^[[:space:]]*$/d' >> ./upload.txt

exit 0
