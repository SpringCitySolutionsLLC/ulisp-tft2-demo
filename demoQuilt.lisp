; demoQuilt.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Clear the screen to black
; Then draw a 10x10 quilt of little random colored squares

(defun demoQuilt nil

  (tft2:init)

  (gfx:clear-screen 0 0 0)

  (dotimes (y 10)
    (dotimes (x 10)
      (gfx:rectangle-filled (* x 10) (* y 10)
                       (+ (* x 10) 10) (+ (* y 10) 10)
                       (random 255) (random 255) (random 255))
      )
    )

  (eval t)

  )

; end of demoQuilt.lisp file
